var pool = require('../db').pool,
	Promise = require('bluebird');

exports.getSomething = function () {
	return new Promise(function (resolve, reject) {
		pool.query('SELECT * FROM x', function (err, rows, fields) {
			if (err)
				reject(err);

			resolve(rows);
		});
	});
};