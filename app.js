var express = require('express'),
	exphbs = require('express3-handlebars'),
	http = require('http'),
	views = require('./routes/views'),
	path = require('path'),
	myService = require('./services/MyService');


var app = express();

var env = app.get('env');

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'handlebars');
app.engine('handlebars', exphbs({
	defaultLayout: 'base',
	layoutsDir: path.join(__dirname, 'views/layouts')
}));
app.use(express.favicon());
app.use(express.bodyParser({ keepExtensions: true, uploadDir: path.join(__dirname,'/file-cache')}));
app.use(express.multipart());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.errorHandler());

app.get('/', views.index);

app.get('/something', function(req, res, next) {
	var uuid = req.params.uuid;

	myService.getSomething()
	.then(function(result) {
		res.end(result);
	})
	.catch(function(err) {
		next(err);
	});
});

app.use(function(err, req, res, next) {
	console.error(err.stack);
	res.send("Something went wrong, sorry!", 500);
});

module.exports = app;