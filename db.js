var mysql = require('mysql');

var pool = mysql.createPool({
	host: 'localhost',
	user: 'user',
	password: 'password',
	database: 'mydb'
});

exports.pool = pool;